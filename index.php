<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Calculator</title>
    <link rel="stylesheet" href="styles.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">
    <script src="https://use.fontawesome.com/1ba4a35d78.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="main wider">
        <div class="display" > 
          <button class="dark_mode_btn"><i class="fa fa-moon-o" aria-hidden="true"></i></button>
          <div class="main-display">
              <h2 id="result"></h2>
            </div>
          <div class="view-history">
            <button id="show_history">
              <i class="fa fa-history" aria-hidden="true"></i>
            </button>
            <p id="history"></p>
          </div>
        </div>
        <div class="buttons">
          <button id="clear">C</button>
          <button id="delete_single_num">
            <i class="fa fa-scissors" aria-hidden="true"></i>
          </button>
          <button id="Normal_btn">/</button>
          <button id="Normal_btn">*</button>
        </div>
        <div class="buttons">
          <button id="Normal_btn">7</button>
          <button id="Normal_btn">8</button>
          <button id="Normal_btn">9</button>
          <button id="Normal_btn">-</button>
        </div>
        <div class="buttons">
          <button id="Normal_btn">4</button>
          <button id="Normal_btn">5</button>
          <button id="Normal_btn">6</button>
          <button id="Normal_btn">+</button>
        </div>
        <div class="buttons">
          <button id="Normal_btn">1</button>
          <button id="Normal_btn">2</button>
          <button id="Normal_btn">3</button>
          <button id="Normal_btn">.</button>
        </div>
        <div class="buttons">
          <button id="Normal_btn">0</button>
          <button id="Normal_btn">00</button>
          <button id="equalTo">=</button>
        </div>
    </div>
    <script src="script.js"></script>
  </body>
</html>
