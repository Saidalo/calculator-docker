document.addEventListener("DOMContentLoaded", function(event) {
    let body = document.querySelector('body');
    let result = document.querySelector('#result');

    let dark_mode_btn = document.querySelector('.dark_mode_btn');
    let clear = document.querySelector('#clear');
    let history = document.querySelector('#history');
    let equalTo = document.querySelector('#equalTo');
    let delete_single_num = document.querySelector('#delete_single_num');
    let Normal_btn = document.querySelectorAll('#Normal_btn');
    let show_history = document.querySelector('#show_history');
    let initial_value = "";
    let db_record = [];
    let operators = ['/', '*', '+', '-'];

    let display = document.querySelector('.display');

    Normal_btn.forEach((Normal_btn, index) => {
        Normal_btn.addEventListener('click', function() {
            let text = this.innerHTML;
            let shouldPut = false;
            if(result.innerHTML.length == 0 && !operators.includes(text)) {
                shouldPut = true;
            } else {
                if(result.innerHTML.length <= 12 && result.innerHTML.length != 0) {
                    if(operators.includes(text) && result.innerHTML[result.innerHTML.length-1] !== text
                    || !(operators.includes(text))) {
                    shouldPut = true;
                    }
                }
            }
            if(shouldPut) {
                initial_value += text;
                result.innerHTML = initial_value;
            }
            

        });
    });

    /*equal to button action*/
    equalTo.addEventListener('click', function() {
        if (result.innerHTML != "") {
            let toSend = result.innerHTML;
            if(['/', '*'].includes(toSend[result.innerHTML.length-1])) {
                toSend += '1';
            } else if (['+', '-'].includes(toSend[result.innerHTML.length-1])) {
                toSend += '0';
            }
          $.ajax({
            url:"Calculator.php",
            type: "POST",
            data: {
              toEvaluate: toSend
            },
            success: function(response) {
              let responseNumber = JSON.parse(response).response;
              db_record = JSON.parse(response).db_record;
              if(responseNumber.toString().length > 5) {
                responseNumber = responseNumber.toPrecision(5);
              }
              display.style.height = "90px";
              history.innerHTML = result.innerHTML;
              result.innerHTML = responseNumber;
              initial_value = responseNumber;
            }
          });
        } else {
            alert('Please enter any number');
        }

    });

    show_history.addEventListener('click', function() {
        let html_content = "";
        let content_height = "90px";
        if(typeof(db_record) == 'boolean' && !db_record) {
                html_content = "database is not available";
        } else {
            db_record.forEach(item => {
                if(item.length > 5) {
                    item = parseFloat(item).toPrecision(5);
                  }
                html_content += item + "<br/>";
            });
            content_height = "150px";
        }
        
        if(html_content !== "") {
            display.style.height = content_height;
            history.innerHTML = html_content;
        }
        
    });

    /*dark_mode*/
    let dark_mode_status = false;
    dark_mode_btn.addEventListener('click', function() {
        body.classList.toggle('dark_mode_active');
        if (dark_mode_status == false) {
            this.innerHTML = '<i class="fa fa-sun-o" aria-hidden="true"></i>';
            dark_mode_status = true;
        } else {
            this.innerHTML = '<i class="fa fa-moon-o" aria-hidden="true"></i>';
            dark_mode_status = false;
        }
    });


    /*clear all number*/
    clear.addEventListener('click', function() {
        result.innerHTML = "";
        initial_value = "";
    });

    /*delete single number*/
    delete_single_num.addEventListener('click', function() {
        result.innerHTML = result.innerHTML.substring(0, result.innerHTML.length - 1);
        initial_value = result.innerHTML;
    });

});
